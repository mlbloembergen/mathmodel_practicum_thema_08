---
title: "Thema 8, assignment week 3"
author: "Liesje Bloembergen en Zoe verstappen"
date: "May 17, 2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# INTRODUCTION
Corticosteroids (CS) are hormones that are used as a medication in several conditions including asthma and COPD. These hormones supress the immune response and the inflammatory reaction in these conditions. The medicine binds to a receptor in the cytosol. This forms an active receptor complex that transfers to the nucleus. In the nucleus the complex supresses the mRNA synthesis of the inflammatory genes. This results in no or a weaker inflammatory reaction. 

With the use of a programmed model the concentration of several receptors and receptor complexes can be visualized. With this visualisation the right concentration of medicine can be determined. 

```{r , out.width = "50%", echo=FALSE}
knitr::include_graphics("model.png")
```
  
*Figure 1: Biological model of the dynamica of glucocorticosteroiden.*

In the image above, the biological model of the dynamica of glucocorticosteroiden. The upper arrow displays the synthesis and degradation of mRNA receptor(mRNA_R). KsRm represents the speed constant of mRNA_R synthesis and Kd_Rm the constant of degradation. The amount of mRNA receptor participates in the calculation of the 
free glucocorticoid receptor density in the cytosol(R). The ratio of sythesis(ks_R) and degredation(kd_R) of the free receptor together with the translaction of a complex back to the cytosol(RfKre), also play a role. The density of MPL-recpetor complex(DR) depents on R and speed constant of synthesis of the MPL-receptor complex(kon). DR together with the speedconstant of the transfer of the complex(kt), eventually plays their part in de amount of MPL-receptor complex in the nucleus(DRN(N)). When the MPL-receptor complex is present in the nucleus, it can supress the synthesis of the mRNA of the inflammatory genes. 
The degradatio of R depends on the speed constantof degradation(kd_R). The 'recovery' of DR(N) is the translocation of the complex back to the cytosol. Eventually a cycle will arise.

In the formulas below is shown how all receptors depend on each other. The formula is the calculation of the difference in mRNA receptor over time. The second formula is the difference in R over time. The third the difference in DR. The last formula is for the difference in DRN.

$$ \frac{dmRNA.r}{dt} = k.srm * (1 - \frac{DRN}{IC.50_rm + DRN} ) - k.drm * mRNA.r$$
$$ \frac{dR}{dt} = k.sr * mRNA.r + Rf * k.re * DRN - k.on * D * R - k.dr * R $$
$$ \frac{dDR}{dt} = k.on * D * R - k.t * DR $$

$$\frac{dDRN}{dt} = kt * DR - k.re * DRN $$

These formulas are used in the model  and can be viewed in the next chapter: RESULTS.

# RESULTS

## ASSIGNMENT WEEK 3.1
In this assignment the result is a plot with the model (last assignment), the experiment (given in a csv file) and the median of the experimental data. For plotting the experimental data it is best to use the median, because if you just use the mean there is a possibility not to have a representative value. There is a big chance to have outliers. Working with the median, gives you the 50% value. So it will give you more information about the data.  

If you increase the dose, the concentration will be increased as well. So the results for the concentration mRNAr will fall deeper when the concentration is higher. The concentration free glucocortoid receptor will be much lower when the concentration is higher. It is possible to see this in the figures below. 

```{r}
data <- read.csv("MPL.csv")
median_MPL_01 <- median(data$MPL_conc[data$dose == 0.1], na.rm = T)
median_MPL_03 <- median(data$MPL_conc[data$dose == 0.3], na.rm = T)
```

```{r}
medians <- aggregate(data[,c("MPL_conc","mRNA","Free_receptor")],
                     list(data$dose, data$time), median, na.rm = T)
names(medians)[1:2] <- c("dose","time")
```

```{r}
library(deSolve)
library('pander')

# parameters of the model
parameters <- function(
  k.s_rm = 2.90,
  IC.50_rm = 26.2,
  k.on = 0.00329,
  k.t = 0.63,
  k.re = 0.57,
  Rf = 0.49,
  k.d_r = 0.0572,
  k.d_rm = 0.612,
  k.s_r = 3.22,
  D = 20*1000/374.471
)
{
  c(k.s_rm = k.s_rm, IC.50_rm = IC.50_rm, k.on = k.on, k.t = k.t, 
    k.re = k.re, Rf = Rf, k.d_r = k.d_r, k.d_rm = k.d_rm, 
    k.s_r = k.s_r, D = D)
}

# functions of model
Model <- function(t, y, parms)
{
  with(as.list(c(parms, y)),
  {
    dmRNA.r <- k.s_rm * (1 - (DRN / (IC.50_rm + DRN))) - k.d_rm * mRNA.r
    dR <- k.s_r * mRNA.r + Rf * k.re * DRN - k.on * D * R - k.d_r * R
    dDR <- k.on * D * R - k.t * DR
    dDRN <- k.t * DR - k.re * DRN
    
    results <- c(dmRNA.r, dR, dDR, dDRN)
    return(list(results))
  })
}

# starting points of model
state <- c(mRNA.r = 4.74, R = 267, DR = 0, DRN = 0)
```

```{r}
library('pander')
times <- seq(0, 168, by=1)

out1 <- ode(times = times, y = state, parms = parameters(D = 14.59 * 1000 / 374.471), 
            func = Model)
out1 <- as.data.frame(out1)

out2 <- ode(times = times, y = state, parms = parameters(D = 39.925 * 1000 / 374.471), 
            func = Model)
out2 <- as.data.frame(out2)

# Plots for dose 0.1
plot(out1$time, out1$mRNA.r, main = "mRNA.r", xlab = "Time (h)", 
   ylab = "Receptor mRNA", type = "l", ylim = c(0,5))
points(data$mRNA ~ data$time)
lines(medians$mRNA[medians$dose == 0.1] ~ medians$time[medians$dose == 0.1], 
      col = "red")
  
plot(out1$time, out1$R, main = "R", xlab = "Time (h)", 
   ylab="Free glucocortoid receptor(cyt)", type = "l", ylim = c(0,400))
points(data$Free_receptor ~ data$time)  
lines(medians$Free_receptor[medians$dose == 0.1] ~ medians$time[medians$dose == 0.1], 
    col = "red")
  
# Plots for dose 0.3
plot(out2$time, out2$mRNA.r, main = "mRNA.r", xlab = "Time (h)", 
   ylab = "Receptor mRNA (fmol/g)", type = "l", ylim = c(0,5))
points(data$mRNA ~ data$time)
lines(medians$mRNA[medians$dose == 0.3] ~ medians$time[medians$dose == 0.3], 
      col = "red")
  
plot(out2$time, out2$R, main = "R", xlab = "Time (h)", 
   ylab = "Free glucocortoid receptor in cytosol (fmol/mg)", type = "l", 
   ylim = c(0,400))
points(data$Free_receptor ~ data$time)  
lines(medians$Free_receptor[medians$dose == 0.3] ~ 
        medians$time[medians$dose == 0.3], col = "red")
```

The results of the model aren't in line with the experimental data. This is because there is a possibility that there is something biological different in the mouse comparing in the biological model. 

## ASSIGNMENT WEEK 3.2
To really understand how the dynamica of glucocorticoids works, the model can be changed.
The first step is changing the model so that the drugs has no influence on the synthesis of receptor mRNA. So the amount of receptor mRNA would be constant. This can be done by changing the formula of receptor mRNA. DRN will change over time. So this variable needs to be removed from the formula:

$$dmRNA.r = k.s(rm) * (1 - \frac {DRN}{IC.50(rm) + DRN}) - k.d(rm) * mRNA.r$$

```{r}
library(deSolve)
library('pander')

# functions of model
Model2 <- function(t, y, parms)
{
  with(as.list(c(parms, y)),
  {
    # Deleted part of the formula for this question
    dmRNA.r <- k.s_rm - k.d_rm * mRNA.r 
    dR <- k.s_r * mRNA.r + Rf * k.re * DRN - k.on * D * R - k.d_r * R
    dDR <- k.on * D * R - k.t * DR
    dDRN <- k.t * DR - k.re * DRN
    
    results <- c(dmRNA.r, dR, dDR, dDRN)
    return(list(results))
  })
}

out3 <- ode(times = times, y = state, parms = parameters(), func = Model2)
out3 <- as.data.frame(out3)

plot(out3$time, out3$DRN, main = "DR(N)", xlab = "Time (h)", 
   ylab = "MPL receptor complex in nucleus (fmol/mg)", type = "l", 
   ylim = c(0,50), col = "red")
lines(out1$time, out1$DRN)
legend('topright', legend = c('adjusted', 'original'), lty = 1:1, 
       col = c("red", "black"))
```

The plot of the adjusted formula has a higher peak for the MPL receptor complex in the nucleus. But it reaches the steady state earlier, than the original formula. 


Second, there can be viewed what happens if the treatment is stopped. This can be done by setting the dose of the medicine to 0 when it has reached the steady state(the equilibrium). There can be seen that when the treatment stops the receptor mRNA and the free glucocorticoid receptor in the cytosol increases. Later they again reach a steady state.

```{r}
t_steady <- 40
times2 <- seq(0, t_steady, by=1)
times3 <- seq(t_steady, 150, by=1)

out4 <- ode(times = times2, y = state, parms = parameters(), func = Model)
out4 <- as.data.frame(out4)
tail.values <- as.matrix(as.vector(tail(out4, n=1)))

out5 <- ode(times = times3, y = tail.values[,2:5], parms = parameters(D=0), func = Model)
out5 <- as.data.frame(out5)

plot(out4$time, out4$mRNA.r, main = "mRNA.r", xlab = "Time (h)", 
   ylab = "Receptor mRNA (fmol/g)", type = "l", ylim = c(0,5), xlim = c(0, 150))
lines(out5$time, out5$mRNA.r, col = "red")

plot(out4$time, out4$R, main = "R", xlab = "Time (h)", 
   ylab = "Free glucocortoid receptor in cytosol (fmol/mg)", type = "l", 
   ylim = c(0,400), xlim = c(0,150))
lines(out5$time, out5$R, col = "red")
```

Different corticosteroids show different association rates from receptors (kon) and different dissociation rates (kre). The concentration of the drug remains the same in the figures below. 

```{r}
# Original
out6 <- ode(times = times, y = state, parms = parameters(), func = Model)
out6 <- as.data.frame(out6)

# Decrease 5
out7 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329/5), func = Model)
out7 <- as.data.frame(out7)

# Decrease 2
out8 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329/2), func = Model)
out8 <- as.data.frame(out8)

# Increase 2 
out9 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329*2), func = Model)
out9 <- as.data.frame(out9)

# Increase 5
out10 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329*5), func = Model)
out10 <- as.data.frame(out10)

# plot for all diferent values
plot(out6$time, out6$mRNA.r, main = "Kon parameters mRNA", xlab = "Time (h)", 
   ylab = "Receptor mRNA (fmol/g)", type = "l", ylim = c(0,5), col = "black")
lines(out7$time, out7$mRNA.r, col = "red")
lines(out8$time, out8$mRNA.r, col = "salmon")
lines(out9$time, out9$mRNA.r, col = "gray")
lines(out10$time, out10$mRNA.r, col = "green")
legend(120, 2, legend = c("original", "devide by 5", "devide by 2", "multiply with 2", "multiply with 5"), lty = 1:1, col = c("black", "red", "salmon", "gray", "green"))
```

In the figure above there are 5 different values for Kon. The smaller the Kon, the bigger the steady state for the mRNA dynamics. 

```{r}
library(deSolve)
library('pander')

# Original
out11 <- ode(times = times, y = state, parms = parameters(), func = Model)
out11 <- as.data.frame(out11)

# Decrease 5
out12 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329/5), func = Model)
out12 <- as.data.frame(out12)

# Decrease 2
out13 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329/2), func = Model)
out13 <- as.data.frame(out13)

# Increase 2
out14 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329*2), func = Model)
out14 <- as.data.frame(out14)

# Increase 5
out15 <- ode(times = times, y = state, parms = parameters(k.on = 0.00329*5), func = Model)
out15 <- as.data.frame(out15)

# plot for all diferent values
plot(out11$time, out11$R, main = "Kon parameters R",xlab = "Time (h)", 
   ylab = "Free glucocortoid receptor in cytosol (fmol/mg)", type = "l", 
   ylim = c(0,300), col = "black")
lines(out12$time, out12$R, col = "red")
lines(out13$time, out13$R, col = "salmon")
lines(out14$time, out14$R, col = "gray")
lines(out15$time, out15$R, col = "green")
legend(115, 300, legend = c("original", "devide by 5", "devide by 2", 
                            "multiply with 2", "multiply with 5"), 
       lty = 1:1, col = c("black", "red", "salmon", "gray", "green"))
```

In the figure above there are 5 different values for Kon. The smaller the Kon, the bigger the steady state for the mRNA dynamic. 

```{r}
library(deSolve)
library('pander')

# Original
out16 <- ode(times = times, y = state, parms = parameters(), func = Model)
out16 <- as.data.frame(out16)

# Decrease 5
out17 <- ode(times = times, y = state, parms = parameters(k.re = 0.57/5), func = Model)
out17 <- as.data.frame(out17)

# Decrease 2
out18 <- ode(times = times, y = state, parms = parameters(k.re = 0.57/2), func = Model)
out18 <- as.data.frame(out18)

# Increase 2
out19 <- ode(times = times, y = state, parms = parameters(k.re = 0.57*2), func = Model)
out19 <- as.data.frame(out19)

# Increase 5
out20 <- ode(times = times, y = state, parms = parameters(k.re = 0.57*5), func = Model)
out20 <- as.data.frame(out20)

# plot for all diferent values
plot(out16$time, out16$mRNA.r, main="Kre parameters mRNA",xlab="Time (h)", 
   ylab="Receptor mRNA (fmol/g)", type='l', ylim = c(0,5), col = "black")
lines(out17$time, out17$mRNA.r, col = "red")
lines(out18$time, out18$mRNA.r, col = "salmon")
lines(out19$time, out19$mRNA.r, col = "gray")
lines(out20$time, out20$mRNA.r, col = "green")
legend('topright', legend = c("original", "devide by 5", "devide by 2", 
                              "multiply with 2", "multiply with 5"), 
       lty = 1:1, col = c("black", "red", "salmon", "gray", "green"))
```

In the figure above there are 5 different values for Kre. The bigger the Kre, the bigger the steady state for the mRNA dynamics. 

```{r}
library(deSolve)
library('pander')

# Original
out21 <- ode(times = times, y = state, parms = parameters(), func = Model)
out21 <- as.data.frame(out21)

# Decrease 5
out22 <- ode(times = times, y = state, parms = parameters(k.re = 0.57/5), func = Model)
out22 <- as.data.frame(out22)

# Decrease 2
out23 <- ode(times = times, y = state, parms = parameters(k.re = 0.57/2), func = Model)
out23 <- as.data.frame(out23)

# Increase 2 
out24 <- ode(times = times, y = state, parms = parameters(k.re = 0.57*2), func = Model)
out24 <- as.data.frame(out24)

# Increase 5
out25 <- ode(times = times, y = state, parms = parameters(k.re = 0.57*5), func = Model)
out25 <- as.data.frame(out25)

# plot for all diferent values
plot(out21$time, out21$R, main = "Kre parameters mRNA",xlab = "Time (h)", 
   ylab = "Receptor mRNA (fmol/g)", type = 'l', ylim = c(0,300), col = "black")
lines(out22$time, out22$R, col = "red")
lines(out23$time, out23$R, col = "salmon")
lines(out24$time, out24$R, col = "gray")
lines(out25$time, out25$R, col = "green")
legend('topright', legend = c("original", "devide by 5", "devide by 2", 
                              "multiply with 2", "multiply with 5"), 
       lty = 1:1, col = c("black", "red", "salmon", "gray", "green"))
```

In the figure above there are 5 different values for Kre. The bigger the Kre, the bigger the steady state for the receptor. 

If the synthesis of the receptor is completely blocked, the parameter Ks_rm needs to be put to zero. In the next plots the black line is for the Ks_rm parameter when it is zero, the pink one when it has it's orginal parameter. All plots show that the steady state is higher when de Ks_rm is zero, this is because if the synthesis of the receptor is blocked, there is less receptor and thus higher chance for inflammatory reaction. 

```{r}
# ks_rm parameter equals to zero
out26 <- ode(times = times, y = state, parms = parameters(k.s_rm = 0), func = Model)
out26 <- as.data.frame(out26)

par(mfrow=c(2,2))
  plot(out26$time, out26$R, main = "R", xlab = "Time (h)", 
       ylab = "Receptor mRNA (fmol/mg)", type = "l", ylim = c(0,300), col = "black")
  lines(out6$time, out6$R, col = "salmon")
  plot(out26$time, out26$mRNA.r, main = "mRNA.r",xlab = "Time (h)", 
       ylab = "Receptor mRNA (fmol/g)", type = "l", ylim = c(0,5), col = "black")
  lines(out6$time, out6$mRNA.r, col = "salmon")
  plot(out26$time, out26$DR, main = "DR", xlab = "Time (h)", 
       ylab = "Receptor mRNA (fmol/mg)", type = "l", ylim = c(0,50), col = "black")
  lines(out6$time, out6$DR, col = "salmon")
  plot(out26$time, out26$DRN, main = "DRN",xlab = "Time (h)", 
       ylab = "Receptor mRNA (fmol/mg)", type = "l", ylim = c(0,50), col = "black")
  lines(out6$time, out6$DRN, col = "salmon")
```

If the mRNA levels are constant, ks_rm and kd_rm need to change. In the plots below there are 4 plots that show that the mRNA levels do have the same steady state after ~ 50 hours. 

```{r}
library(deSolve)
library('pander')

# Orginal 
out26 <- ode(times = times, y = state, parms = parameters(), func = Model)
out26 <- as.data.frame(out26)

# ks_Rm = 2.9/5 and kd_Rm=2.9/5/4.74
out27 <- ode(times = times, y = state, 
             parms = parameters(k.s_rm = 2.9/5, k.d_rm=2.9/5/4.74), func = Model)
out27 <- as.data.frame(out27)

# ks_Rm = 2.9/2 and kd_Rm=2.9/2/4.74
out28 <- ode(times = times, y = state, 
             parms = parameters(k.s_rm = 2.9/2, k.d_rm=2.9/2/4.74), func = Model)
out28 <- as.data.frame(out28)

# ks_Rm = 2.9*2 and kd_Rm=2.9*2/4.74
out29 <- ode(times = times, y = state, 
             parms = parameters(k.s_rm = 2.9*2, k.d_rm=2.9*2/4.74), func = Model)
out29 <- as.data.frame(out29)

# ks_Rm = 2.9*5 and kd_Rm=2.9*5/4.74
out30 <- ode(times = times, y = state, 
             parms = parameters(k.s_rm = 2.9*5, k.d_rm=2.9*5/4.74), func = Model)
out30 <- as.data.frame(out30)

# plot for all diferent values
plot(out26$time, out26$mRNA.r, main = "Kre parameters mRNA",xlab = "Time (h)", 
   ylab = "Receptor mRNA (fmol/g)", type = 'l', ylim = c(0,5), col = "black")
lines(out27$time, out27$mRNA.r, col = "red")
lines(out28$time, out28$mRNA.r, col = "salmon")
lines(out29$time, out29$mRNA.r, col = "gray")
lines(out30$time, out30$mRNA.r, col = "green")
legend('bottomright', legend = c("original", "ks_Rm = 2.9/5 and kd_Rm=2.9/5/4.74", 
                              "ks_Rm = 2.9/2 and kd_Rm=2.9/2/4.74", 
                              "ks_Rm = 2.9*2 and kd_Rm=2.9*2/4.74", 
                              "ks_Rm = 2.9*5 and kd_Rm=2.9*5/4.74"), 
       lty = 1:1, col = c("black", "red", "salmon", "gray", "green"))
```





