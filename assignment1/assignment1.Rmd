---
title: "Assignment 1"
author: "Liesje Bloembergen"
date: "May 7, 2019"
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### OPDRACHT 1

$$\frac{dR}{dt}= -rR + m$$

1) De parameters in dit geval is de transciptie die je toevoegt en het percentage wat erafgaat, oftewel: 
-r (afbraakratio) en +m (synthese).

2)
https://science.sciencemag.org/content/343/6169/422 


3)
In de code hieronder is het biologische proces schematisch weergegeven. 
```{r}
knitr::include_graphics("proces_1.png")
```

De vertaling van het biologische proces naar de formule: 
$$\frac{dR}{dt}= -rR + m$$

We noemen dR/dt de afgeleide functie, dit is de functie die de verandering per tijdseenheid beschrijft.
-r en +m zijn de parameters en de transcriptie wordt vermenigvuldigd met de afbraak (-r). 

4) De return waarde van het model is: return(list(c(dR))).
Je returnt een list, omdat de verwachting is dat er meerdere waarden (oftewel de resultaten) uit komen en deze vervolgens weergegeven kunnen worden in een lijst. De resultaten zijn de afgeleides, en niet de R waarde zelf. Je returnt de afgeleide, omdat dit de verandering beschrijft (per tijdseenheid). 

### OPDRACHT 2

```{r}
library(deSolve)

parameters.state <- c(afbraak = 2, synthese = 200) 
parameters.increas <- c(afbraak = 2, synthese = 250) 
parameters.decreas <- c(afbraak = 2, synthese = 150)

#define model
transcription <- function(t,R,parms){
  with(as.list(c(parms)),{
    dR <- (-afbraak*R) + synthese
    return(list(c(dR)))
    }
    )
}

#initial state
state <- c(state = 100)

#define time sequence you want to run the model
times <- seq(0, 6,  by = 0.1)

# run simulation using continuous approach
out.state  <- ode(times = times, y = state, parms = parameters.state, func = transcription, method = "euler")
out.increas <- ode(times = times, y = state, parms = parameters.increas, func = transcription, method = "euler")
out.decreas <- ode(times = times, y = state, parms = parameters.decreas, func = transcription, method = "euler")

plot(out.state, out.increas, out.decreas, xlab = "timepoints (in sec)", ylab = "number of mRNA transcripts", col = c("red", "green", "blue"), lty = 1)

legend(x = 3.5, y = 120, legend = c("steady-state", "increasing over time", "decreasing over time"), fill = c("red", "green", "blue") )

```

